﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(CharacterController))]

public class firstPersonController : MonoBehaviour {

    public float movementspeed = 5.0f;
    public float mouseSenitivity = 5.0f;

    float varticalRotation = 0.0f;
    float upDownRange = 60.0f;

    float verticalVelocity = 0;

    public float jumpSpeed = 7.0f;

    CharacterController characterController;

    


    // Use this for initialization
    void Start () {
        //Cursor.lockState = CursorLockMode.Locked;
        //Cursor.visible = false;

        characterController = GetComponent<CharacterController>();

    }
	
	// Update is called once per frame
	void Update () {

        

        //Rotation
        float RotateLeftRight = Input.GetAxis("Mouse X") * mouseSenitivity;
        transform.Rotate(0, RotateLeftRight, 0);

        float RotateUpDown = Input.GetAxis("Mouse Y") * mouseSenitivity;
        varticalRotation = varticalRotation - RotateUpDown;
        varticalRotation = Mathf.Clamp(varticalRotation, -upDownRange, upDownRange);
        Camera.main.transform.localRotation = Quaternion.Euler(varticalRotation, 0, 0);

        //Camera.main.transform.Rotate(-RotateUpDown, 0, 0);
        


        //Movement
        float forwardSpeed =  Input.GetAxis("Vertical") * movementspeed;
        float sideSpeed =   Input.GetAxis("Horizontal") * movementspeed;



        //Vector3 speed = new Vector3(sideSpeed, Physics.gravity.y, forwardSpeed);

        verticalVelocity += Physics.gravity.y * Time.deltaTime;

        if (characterController.isGrounded && Input.GetButton("Jump"))
        {
            verticalVelocity = jumpSpeed;
        }

        Vector3 speed = new Vector3(sideSpeed, verticalVelocity, forwardSpeed);

        speed = transform.rotation * speed;



        //cc.SimpleMove(speed);
        characterController.Move(speed * Time.deltaTime);
        
    }
}
